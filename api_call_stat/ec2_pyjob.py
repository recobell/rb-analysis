#-*- coding: utf-8 -*-

import os
import shutil
import sys
import glob
import re
import json

import time
import datetime

import boto3
import botocore
import requests

import pandas as pd

from functools import wraps

def print_time_spent(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        func_name = str(func).split()[-3]
        print('*** <{}> started ***'.format(func_name))
        start_d_time = datetime.datetime.now()
        start_time = time.time()
        output = func(*args, **kwargs)
        print('*** %s finished: took %s (%.3f seconds). ***' % (func_name, str((datetime.datetime.now() - start_d_time))[:7], (time.time() - start_time)))
        return output
    return decorated


def get_time_prefix():
    utcnow = datetime.datetime.utcnow()
    yesterday = utcnow - datetime.timedelta(days=1)
    year = str(yesterday.year)
    month = str(yesterday.month) if yesterday.month > 9 else ('0' + str(yesterday.month))
    day = str(yesterday.day) if yesterday.day > 9 else ('0' + str(yesterday.day))
    time_prefix = year + '/' + month + '/' + day + '/'
    return time_prefix


def get_dist_prefix(dist, time_prefix):
    dist_prefix = dist + time_prefix
    return dist_prefix



# download

def create_a_connection(public_key, secret_key):
    s3resource = boto3.resource('s3', 
                                aws_access_key_id = public_key, 
                                aws_secret_access_key = secret_key)
    s3client = boto3.client('s3',
                           aws_access_key_id = public_key,
                           aws_secret_access_key = secret_key)
    return s3resource, s3client


def access_a_bucket(s3resource, target_bucket):
    bucket = s3resource.Bucket(target_bucket)
    exists = True
    try:
        s3resource.meta.client.head_bucket(Bucket=target_bucket)
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            exists = False
    print('bucket exists: %s' % (exists))
    if exists:
        return bucket


def mv_dir(dir_to):
    if os.getcwd()[0] != dir_to[0]:
        os.system(dir_to[0])
    os.chdir(dir_to)


def refresh_dir(directory):
    remove_dir(directory)
    os.mkdir(directory)

def remove_dir(directory):
    try:
        shutil.rmtree(directory)
    except Exception as error:
        print(error)


@print_time_spent
def download_dir(client, resource, dist, local, bucket):    
    paginator = s3client.get_paginator('list_objects')
    operation_parameters = {'Bucket': bucket,
                            'Delimiter': '/',
                            'Prefix': dist}
    page_iterator = paginator.paginate(**operation_parameters)


    for result in page_iterator:
        if result.get('CommonPrefixes') is not None:
            for subdir in result.get('CommonPrefixes'):
                download_dir(s3client, s3resource, subdir.get('Prefix'), local, bucket)
        if result.get('Contents') is not None:
            for file in result.get('Contents'):
                if not os.path.exists(os.path.dirname(local + os.sep + file.get('Key'))):
                    os.makedirs(os.path.dirname(local + os.sep + file.get('Key')))
                if not file.get('Key').endswith('/'):
                    if not os.path.exists(local + os.sep + file.get('Key')):
                        s3resource.meta.client.download_file(bucket, file.get('Key'), local + '/' + file.get('Key'))
                    else:
                        print("%s already exists" % (local + os.sep + file.get('Key')))


# get results

def get_file_names(directory, prefix):
    file_name_li = sorted(glob.glob(directory+'/'+prefix+'*', recursive=True))
    return file_name_li

@print_time_spent
def get_cuid_rectype_dic(file_name_li):
    dic = dict()
    total_lines = 0
    error_lines = 0
    for file_name in file_name_li:
        logs = parsing(file_name)
        tmp_total_lines, tmp_error_lines = get_cuid_rectype_pair(logs, dic)
        total_lines += tmp_total_lines
        error_lines += tmp_error_lines
    print('error / total : %s / %s' % (error_lines, total_lines))
    return dic

# for get_cuid_rectype_dic
def parsing(file_name):                
    with open(file_name, "r", encoding='latin-1') as f:
        logs = []
        for line in f:
            if line[-1] == "\n":
                logs.append(line[:-1])
            else:
                logs.append(line)     
    return logs

def get_cuid_rectype_pair(logs, dic):
    p_cuid = re.compile(r"(cuid=).{36}")
    p_rectype = re.compile(r"(/rec/.{1,8}\/)|(/rec/.{1,8}\?)")
    tmp_total_lines = 0
    tmp_error_lines = 0
    for log in logs:
        try:
            target = log.split()[12]
            cuid = p_cuid.search(target).group()[5:]
            rectype = p_rectype.search(target).group()[5:-1]
        except:
            tmp_error_lines += 1
            continue
        try:
            dic[cuid]
        except:
            dic[cuid] = dict()
        try:
            dic[cuid][rectype] += 1
        except:
            dic[cuid][rectype] = 1
        tmp_total_lines += 1
    return tmp_total_lines, tmp_error_lines


def load_json(url):
    rb_json = requests.get(url).json()
    return rb_json


def get_merged_dic(json, dic):
    cuids = sorted(dic.keys())
    dic_merged = dict()
    for cuid in cuids:
        try:
            name = json['cuids']['siteMap'][cuid]['name']
            category = json['cuids']['siteMap'][cuid]['category']
            dic_merged[cuid] = {'name':name, 'category':category, 'rectypes':dic[cuid]}
            dic_merged[cuid]['total'] = sum(dic_merged[cuid]['rectypes'].values())
        except:
            continue
    return dic_merged


def save_json(dic, save_json_name):
    file_name = save_json_name+".json"
    with open(file_name, 'w', encoding='utf-8') as f:
        json.dump(dic, f, indent=4, ensure_ascii=False)


def get_rectype_df(dic):
    cuids = sorted(dic.keys())
    # get rectype set
    rectype_set = set()
    for cuid in cuids:
        rectype_set = rectype_set | set(dic[cuid]['rectypes'].keys())
    rectype_set = sorted(rectype_set)

    # get rectype matrix
    rectype_arr = []
    for cuid in cuids:
        tmp = []
        for rectype in rectype_set:
            if rectype in dic[cuid]['rectypes']:
                tmp.append('0')
            else:
                tmp.append(' ')
        rectype_arr.append(tmp)

    df_rectype = pd.DataFrame(rectype_arr, index=cuids, columns=rectype_set)
    return df_rectype


def get_meta_df(dic):
    cuids = sorted(dic.keys())
    names = []
    categorys = []
    for cuid in cuids:
        names.append(dic[cuid]['name'])
        categorys.append(dic[cuid]['category'])
    df_meta = pd.DataFrame({'cuid':cuids, 'name':names, 'category':categorys}, index=cuids)
    return df_meta


def concat_dfs_horizontally(df1, df2):
    df_merged = pd.concat([df1, df2], axis=1).sort_values(by=['category'])
    return df_merged


def df2csv(df, save_csv_name):
    df.to_csv(save_csv_name+"_euc_kr.csv", index=False, encoding='euc_kr')
    df.to_csv(save_csv_name+"_utf-8.csv", index=False, encoding='utf-8')


# upload
def get_file_list(local_path):
    file_list = os.listdir(local_path)
    return file_list

def get_file_path_list_from(local_path, file_list):
    file_path_list_from = [(local_path + file) for file in file_list]
    return file_path_list_from



def get_file_path_list_to(folder, time_prefix, file_list):
    file_path_list_to = [(folder + time_prefix + file) for file in file_list]
    return file_path_list_to


def upload_files(s3client, file_path_list_from, bucket, file_path_list_to):
    for i in range(len(file_path_list_from)):
        try:
            s3client.upload_file(file_path_list_from[i], bucket, file_path_list_to[i])
        except Exception as error:
            print(error)
            continue


if __name__=='__main__':

    # parameters
    aws_key = {
                'public_key': 'AKIAINS5BEHVPHMF3KKQ',
                'secret_key': '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'
            }
    public_key = aws_key['public_key']
    secret_key = aws_key['secret_key']

    bucket_from = 'rb-api-apne1-log'
    bucket_to = 'rb-analysis'

    dist_from = 'AWSLogs/062310586378/elasticloadbalancing/ap-northeast-1/'
    dist_to = 'api_call/'

    dir_working = 'C:\\works\\api_call_stat'
    dir_local_raw = 'data_log_raw'
    dir_result = 'result/'
    path_local_result = './result/'
    save_name = './result/cuid_rectype'
    url_customer_info = 'http://rblogger-receiver-apne1.elasticbeanstalk.com/rest/state'

    # settings
    sys.setrecursionlimit(20000)
    time_prefix = get_time_prefix()
    file_name_prefix = get_dist_prefix(dist_from, time_prefix)
    print(time_prefix)
    print(file_name_prefix)

    # download
    s3resource, s3client = create_a_connection(public_key, secret_key)
    s3bucket = access_a_bucket(s3resource, bucket_from)
    #mv_dir(dir_working)
    refresh_dir(dir_local_raw)
    download_dir(s3client, s3resource, file_name_prefix, dir_local_raw, bucket_from)

    # get stat
    refresh_dir(dir_result)
    file_name_li = get_file_names(dir_local_raw, file_name_prefix)
    dic_rectype = get_cuid_rectype_dic(file_name_li)
    rb_json = load_json(url_customer_info)
    dic_merged = get_merged_dic(rb_json, dic_rectype)
    save_json(dic_merged, save_name)

    # get df
    df_rectype = get_rectype_df(dic_merged)
    df_meta = get_meta_df(dic_merged)
    df_merged = concat_dfs_horizontally(df_meta, df_rectype)
    df2csv(df_merged, save_name)

    # upload
    s3bucket = access_a_bucket(s3resource, bucket_to)
    file_list = get_file_list(path_local_result)
    file_path_list_from = get_file_path_list_from(path_local_result, file_list)
    file_path_list_to = get_file_path_list_to(dist_to, time_prefix, file_list)
    upload_files(s3client, file_path_list_from, bucket_to, file_path_list_to)