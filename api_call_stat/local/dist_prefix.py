import time
import datetime


def get_time_prefix():
    utcnow = datetime.datetime.utcnow()
    yesterday = utcnow - datetime.timedelta(days=1)
    year = str(yesterday.year)
    month = str(yesterday.month) if yesterday.month > 9 else ('0' + str(yesterday.month))
    day = str(yesterday.day) if yesterday.day > 9 else ('0' + str(yesterday.day))
    time_prefix = year + '/' + month + '/' + day + '/'
    return time_prefix


def get_dist_prefix(dist, time_prefix):
    dist_prefix = dist + time_prefix
    return dist_prefix