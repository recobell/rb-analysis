#-*- coding: utf-8 -*-

import os

def job():
    print("installing dependencies")
    os.system('pip install -r requirements.txt')
    print("Running schedule.")
    os.system('python 01_download_from_s3.py')
    print('process 1 finished')
    os.system('python 02_move_dir.py')
    print('process 2 finished')
    os.system('python 03_get_cuid_rectype_dic.py')
    print('process 3 finished')
    os.system('python 04_upload_to_s3.py')
    print('process 4 finished')
    return

job()