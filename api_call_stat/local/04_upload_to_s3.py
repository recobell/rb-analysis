#-*- coding: utf-8 -*-

import boto3
import botocore
import os
import shutil
import sys

import time
import datetime

import aws_key
import dist_prefix


def create_a_connection(public_key, secret_key):
    s3resource = boto3.resource('s3', 
                                aws_access_key_id = public_key, 
                                aws_secret_access_key = secret_key)
    s3client = boto3.client('s3',
                           aws_access_key_id = public_key,
                           aws_secret_access_key = secret_key)
    return s3resource, s3client


def access_a_bucket(s3resource, target_bucket):
    bucket = s3resource.Bucket(target_bucket)
    exists = True
    try:
        s3resource.meta.client.head_bucket(Bucket=target_bucket)
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            exists = False
    print('bucket exists: %s' % (exists))
    if exists:
        return bucket


def get_file_list(local_path):
    file_list = os.listdir(local_path)
    return file_list

def get_file_path_list_from(local_path, file_list):
    file_path_list_from = [(local_path + file) for file in file_list]
    return file_path_list_from

def get_file_path_list_to(dist_prefix, file_list):
    file_path_list_to = [(dist_prefix + file) for file in file_list]
    return file_path_list_to


def upload_files(s3client, file_path_list_from, bucket, file_path_list_to):
    for i in range(len(file_path_list_from)):
        try:
            s3client.upload_file(file_path_list_from[i], bucket, file_path_list_to[i])
        except Exception as error:
            print(error)
            continue


if __name__=='__main__':
    # params
    aws_key = aws_key.aws_key
    public_key = aws_key['public_key']
    secret_key = aws_key['secret_key']
    bucket = 'rb-analysis'
    dist = 'api_call/'
    local_path = './result/'

    # process
    s3resource, s3client = create_a_connection(public_key, secret_key)
    s3bucket = access_a_bucket(s3resource, bucket)
    time_prefix = dist_prefix.get_time_prefix()
    dist_prefix = dist_prefix.get_dist_prefix(dist, time_prefix)
    file_list = get_file_list(local_path)
    file_path_list_from = get_file_path_list_from(local_path, file_list)
    file_path_list_to = get_file_path_list_to(dist_prefix, file_list)
    upload_files(s3client, file_path_list_from, bucket, file_path_list_to)