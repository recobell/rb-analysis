#-*- coding: utf-8 -*-

import boto3
import botocore
import os
import shutil
import sys

import time
import datetime

import aws_key
import dist_prefix


def create_a_connection(public_key, secret_key):
    s3resource = boto3.resource('s3', 
                                aws_access_key_id = public_key, 
                                aws_secret_access_key = secret_key)
    s3client = boto3.client('s3',
                           aws_access_key_id = public_key,
                           aws_secret_access_key = secret_key)
    return s3resource, s3client


def access_a_bucket(s3resource, target_bucket):
    bucket = s3resource.Bucket(target_bucket)
    exists = True
    try:
        s3resource.meta.client.head_bucket(Bucket=target_bucket)
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 404:
            exists = False
    print('bucket exists: %s' % (exists))
    if exists:
        return bucket


def mv_dir(dir_to):
    if os.getcwd()[0] != dir_to[0]:
        os.system(dir_to[0])
    os.chdir(dir_to)


def refresh_dir(local):
    try:
        shutil.rmtree(local)
    except Exception as error:
        print(error)
    os.mkdir(local)


def download_dir(client, resource, dist, local, bucket):    
    paginator = s3client.get_paginator('list_objects')
    operation_parameters = {'Bucket': target_bucket,
                            'Delimiter': '/',
                            'Prefix': dist}
    page_iterator = paginator.paginate(**operation_parameters)


    for result in page_iterator:
        if result.get('CommonPrefixes') is not None:
            for subdir in result.get('CommonPrefixes'):
                download_dir(s3client, s3resource, subdir.get('Prefix'), local, bucket)
        if result.get('Contents') is not None:
            for file in result.get('Contents'):
                if not os.path.exists(os.path.dirname(local + os.sep + file.get('Key'))):
                    os.makedirs(os.path.dirname(local + os.sep + file.get('Key')))
                if not file.get('Key').endswith('/'):
                    if not os.path.exists(local + os.sep + file.get('Key')):
                        print("start  %s " % file.get('Key') )
                        s3resource.meta.client.download_file(bucket, file.get('Key'), local + '/' + file.get('Key'))
                        print("downloaded %s " % file.get('Key') )
                    else:
                        print("%s already exists" % (local + os.sep + file.get('Key')))


if __name__=='__main__':
    print('*** I/O started ***')
    start_d_time_total = datetime.datetime.now()
    start_time_total = time.time()

    # parameters
    aws_key = aws_key.aws_key
    public_key = aws_key['public_key']
    secret_key = aws_key['secret_key']
    target_bucket = 'rb-api-apne1-log'
    #dir_to = '/project_data'
    dir_to = './'
    local = 'data_log_raw'
    dist = 'AWSLogs/062310586378/elasticloadbalancing/ap-northeast-1/'

    # process
    sys.setrecursionlimit(20000)
    s3resource, s3client = create_a_connection(public_key, secret_key)
    s3bucket = access_a_bucket(s3resource, target_bucket)
    time_prefix = dist_prefix.get_time_prefix()
    dist_prefix = dist_prefix.get_dist_prefix(dist, time_prefix)
    print(dist_prefix)
    mv_dir(dir_to)
    refresh_dir(local)
    download_dir(s3client, s3resource, dist_prefix, local, target_bucket)

    print('*** I/O finished: %s (%.3f seconds). ***' % (str((datetime.datetime.now() - start_d_time_total))[:7], (time.time() - start_time_total)))
