#-*- coding: utf-8 -*-

import schedule
import time
import os

def job(t):
    print("Running schedule.", t)
    os.system('python 01_download_from_s3.py')
    os.system('python 02_move_dir.py')
    os.system('python 03_get_cuid_rectype_dic.py')
    os.system('python 04_upload_to_s3.py')
    return

schedule.every().day.at("09:05").do(job,'It is 09:05')

while True:
    schedule.run_pending()
    time.sleep(60)
