#-*- coding: utf-8 -*-

import os
import shutil

import time
import datetime

import dist_prefix


def refresh_dir(dir_to):
    try:
        shutil.rmtree(dir_to)
    except Exception as error:
        print(error)
    os.mkdir(dir_to)


def move_files(local, dist_prefix, dir_to):
    dir_from = './' + local + '/' + dist_prefix
    file_list = os.listdir(dir_from)
    for file in file_list:
        try:
            shutil.move((dir_from+file), (dir_to+file))
        except Exception as error:
            print(error)
            continue


if __name__=='__main__':

    # parameters
    local = './data_log_raw'
    dir_to = 'data_log/'
    dist = 'AWSLogs/062310586378/elasticloadbalancing/ap-northeast-1/'

    # process
    time_prefix = dist_prefix.get_time_prefix()
    dist_prefix = dist_prefix.get_dist_prefix(dist, time_prefix)
    refresh_dir(dir_to)
    move_files(local, dist_prefix, dir_to)