#-*- coding: utf-8 -*-

import glob
import re
import json
import os
import shutil
import sys
import requests

import numpy as np
import pandas as pd

import time
import datetime


def refresh_dir(dir_result):
    try:
        shutil.rmtree(dir_result)
    except Exception as error:
        print(error)
    os.mkdir(dir_result)


def get_file_names(prefix):
    file_name_li = sorted(glob.glob(prefix+'*', recursive=True))
    return file_name_li

def get_cuid_rectype_dic(file_name_li):
	dic = dict()
	total_lines = 0
	error_lines = 0
	for file_name in file_name_li:
		logs = parsing(file_name)
		tmp_total_lines, tmp_error_lines = get_cuid_rectype_pair(logs, dic)
		total_lines += tmp_total_lines
		error_lines += tmp_error_lines
	print('error / total : %s / %s' % (error_lines, total_lines))
	return dic

# for get_cuid_rectype_dic
def parsing(file_name):                
    with open(file_name, "r", encoding='latin-1') as f:
        logs = []
        for line in f:
            if line[-1] == "\n":
                logs.append(line[:-1])
            else:
                logs.append(line)     
    return logs

def get_cuid_rectype_pair(logs, dic):
    p_cuid = re.compile(r"(cuid=).{36}")
    p_rectype = re.compile(r"(/rec/.{1,8}\/)|(/rec/.{1,8}\?)")
    tmp_total_lines = 0
    tmp_error_lines = 0
    for log in logs:
        try:
            target = log.split()[12]
            cuid = p_cuid.search(target).group()[5:]
            rectype = p_rectype.search(target).group()[5:-1]
        except:
            tmp_error_lines += 1
            continue
        try:
        	dic[cuid]
        except:
        	dic[cuid] = dict()
        try:
        	dic[cuid][rectype] += 1
        except:
        	dic[cuid][rectype] = 1
        tmp_total_lines += 1
    return tmp_total_lines, tmp_error_lines


def load_json(url):
    rb_json = requests.get(url).json()
    return rb_json


def get_merged_dic(json, dic):
	cuids = sorted(dic.keys())
	dic_merged = dict()
	for cuid in cuids:
		try:
			name = json['cuids']['siteMap'][cuid]['name']
			category = json['cuids']['siteMap'][cuid]['category']
			dic_merged[cuid] = {'name':name, 'category':category, 'rectypes':dic[cuid]}
			dic_merged[cuid]['total'] = sum(dic_merged[cuid]['rectypes'].values())
		except:
			continue
	return dic_merged


def save_json(dic, save_json_name):
	file_name = save_json_name+".json"
	with open(file_name, 'w', encoding='utf-8') as f:
		json.dump(dic, f, indent=4, ensure_ascii=False)


def get_rectype_df(dic):
	cuids = sorted(dic.keys())
	# get rectype set
	rectype_set = set()
	for cuid in cuids:
		rectype_set = rectype_set | set(dic[cuid]['rectypes'].keys())
	rectype_set = sorted(rectype_set)

	# get rectype matrix
	rectype_arr = []
	for cuid in cuids:
		tmp = []
		for rectype in rectype_set:
			if rectype in dic[cuid]['rectypes']:
				tmp.append(dic[cuid]['rectypes'][rectype])
			else:
				tmp.append(0)
		rectype_arr.append(tmp)

	df_rectype = pd.DataFrame(rectype_arr, index=cuids, columns=rectype_set)
	return df_rectype


def get_meta_df(dic):
	cuids = sorted(dic.keys())
	names = []
	categorys = []
	for cuid in cuids:
		names.append(dic[cuid]['name'])
		categorys.append(dic[cuid]['category'])
	df_meta = pd.DataFrame({'cuid':cuids, 'name':names, 'category':categorys}, index=cuids)
	return df_meta


def concat_dfs_horizontally(df1, df2):
	df_merged = pd.concat([df1, df2], axis=1).sort_values(by=['category'])
	return df_merged


def df2csv(df, save_csv_name):
	df.to_csv(save_csv_name+"_euc_kr.csv", index=False, encoding='euc_kr')
	df.to_csv(save_csv_name+"_utf-8.csv", index=False, encoding='utf-8')


if __name__ == '__main__':
	start_d_time = datetime.datetime.now()
	start_time = time.time()
	print('*** process started ***')

	# variables
	prefix = './data_log/'
	url = 'http://rblogger-receiver-apne1.elasticbeanstalk.com/rest/state'
	save_json_name = './result/cuid_rectype'
	save_csv_name = './result/cuid_rectype'
	dir_result = 'result/'

	# process
	refresh_dir(dir_result)
	file_name_li = get_file_names(prefix)
	dic_rectype = get_cuid_rectype_dic(file_name_li)
	rb_json = load_json(url)
	dic_merged = get_merged_dic(rb_json, dic_rectype)
	save_json(dic_merged, save_json_name)

	# get df
	df_rectype = get_rectype_df(dic_merged)
	df_meta = get_meta_df(dic_merged)
	df_merged = concat_dfs_horizontally(df_meta, df_rectype)
	df2csv(df_merged, save_csv_name)

	print('*** process finished: took %s (%.3f seconds). ***' % (str((datetime.datetime.now() - start_d_time))[:7], (time.time() - start_time)))